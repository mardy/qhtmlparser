/*
 * Copyright (C) 2021 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of QHtmlParser.
 *
 * QHtmlParser is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QHtmlParser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with QHtmlParser.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IT_MARDY_HTML_PARSER_H
#define IT_MARDY_HTML_PARSER_H

#include <QObject>
#include <QScopedPointer>
#include <QString>
#include <QVector>

namespace it {
namespace mardy {
namespace Html {

class ParserPrivate;
class Parser
{
    Q_GADGET

public:
    enum Option {
        None = 0,
        ConvertCharRefs = 1 << 0,
    };
    Q_DECLARE_FLAGS(Options, Option)
    Q_FLAG(Options)

    struct Attribute {
        QString name;
        QString value;
    };
    struct Attributes: public QVector<Attribute> {
        QString value(const QString &name,
                      const QString &defaultValue = QString()) const {
            for (const Attribute &a: *this) {
                if (a.name == name) return a.value;
            }
            return defaultValue;
        }
    };

    Parser(Options options);
    virtual ~Parser();

    void reset();

    void feed(const QString &data);

    void close();

    QString startTagText() const;

protected:
    virtual void handleStartEndTag(const QString &tag, const Attributes &attrs);
    virtual void handleStartTag(const QString &tag, const Attributes &attrs);
    virtual void handleEndTag(const QString &tag);
    virtual void handleCharRef(const QString &name);
    virtual void handleEntityRef(const QString &name);
    virtual void handleData(const QString &data);
    virtual void handleComment(const QString &data);
    virtual void handleDecl(const QString &decl);
    virtual void handlePi(const QString &data);
    virtual void handleUnknownDecl(const QString &data);

    virtual void handleError(const QString &message);

private:
    Q_DECLARE_PRIVATE(Parser)
    QScopedPointer<ParserPrivate> d_ptr;
};

}}} // namespace

#endif // IT_MARDY_HTML_PARSER_H
