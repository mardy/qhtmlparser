/*
 * Copyright (C) 2021 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of QHtmlParser.
 *
 * QHtmlParser is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QHtmlParser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with QHtmlParser.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "html.h"

#include "entities.h"

#include <QChar>
#include <QDebug>
#include <QRegularExpression>
#include <QXmlStreamReader>


namespace {

const QRegularExpression s_charRef {
    QStringLiteral(R"(&(#[0-9]+;?)"
                   R"(|#[xX][0-9a-fA-F]+;?)"
                   R"(|[^\t\n\f <&#;]{1,32};?))")
};

// see http://www.w3.org/TR/html5/syntax.html#tokenizing-character-references
const QHash<uint, uint> s_invalidCharRefs = {
    { 0x00, U'\ufffd' },  // REPLACEMENT CHARACTER
    { 0x0d, '\r' },      // CARRIAGE RETURN
    { 0x80, U'\u20ac' },  // EURO SIGN
    { 0x81, '\x81' },    // <control>
    { 0x82, U'\u201a' },  // SINGLE LOW-9 QUOTATION MARK
    { 0x83, U'\u0192' },  // LATIN SMALL LETTER F WITH HOOK
    { 0x84, U'\u201e' },  // DOUBLE LOW-9 QUOTATION MARK
    { 0x85, U'\u2026' },  // HORIZONTAL ELLIPSIS
    { 0x86, U'\u2020' },  // DAGGER
    { 0x87, U'\u2021' },  // DOUBLE DAGGER
    { 0x88, U'\u02c6' },  // MODIFIER LETTER CIRCUMFLEX ACCENT
    { 0x89, U'\u2030' },  // PER MILLE SIGN
    { 0x8a, U'\u0160' },  // LATIN CAPITAL LETTER S WITH CARON
    { 0x8b, U'\u2039' },  // SINGLE LEFT-POINTING ANGLE QUOTATION MARK
    { 0x8c, U'\u0152' },  // LATIN CAPITAL LIGATURE OE
    { 0x8d, '\x8d' },    // <control>
    { 0x8e, U'\u017d' },  // LATIN CAPITAL LETTER Z WITH CARON
    { 0x8f, '\x8f' },    // <control>
    { 0x90, '\x90' },    // <control>
    { 0x91, U'\u2018' },  // LEFT SINGLE QUOTATION MARK
    { 0x92, U'\u2019' },  // RIGHT SINGLE QUOTATION MARK
    { 0x93, U'\u201c' },  // LEFT DOUBLE QUOTATION MARK
    { 0x94, U'\u201d' },  // RIGHT DOUBLE QUOTATION MARK
    { 0x95, U'\u2022' },  // BULLET
    { 0x96, U'\u2013' },  // EN DASH
    { 0x97, U'\u2014' },  // EM DASH
    { 0x98, U'\u02dc' },  // SMALL TILDE
    { 0x99, U'\u2122' },  // TRADE MARK SIGN
    { 0x9a, U'\u0161' },  // LATIN SMALL LETTER S WITH CARON
    { 0x9b, U'\u203a' },  // SINGLE RIGHT-POINTING ANGLE QUOTATION MARK
    { 0x9c, U'\u0153' },  // LATIN SMALL LIGATURE OE
    { 0x9d, '\x9d' },    // <control>
    { 0x9e, U'\u017e' },  // LATIN SMALL LETTER Z WITH CARON
    { 0x9f, U'\u0178' },  // LATIN CAPITAL LETTER Y WITH DIAERESIS
};

QString lookUpHtml5(const QByteArray &key) {
    const auto i = s_html5.find(key);
    if (i != s_html5.end()) {
        return QChar(i.value());
    }
    const auto j = s_html5Strings.find(key);
    if (j != s_html5Strings.end()) {
        return j.value();
    }
    return QString();
}

QString replaceCharRef(const QRegularExpressionMatch &m)
{
    QStringRef s = m.capturedRef(1);
    if (s[0] == '#') {
        // numeric charref
        uint num = 0;
        if (s.endsWith(';')) {
            s.chop(1); // remove ending ';'
        }
        if (QString("xX").contains(s[1])) {
            num = s.mid(2).toUInt(nullptr, 16);
        } else {
            num = s.mid(1).toUInt();
        }
        if (s_invalidCharRefs.contains(num)) {
            return QChar(s_invalidCharRefs[num]);
        }
        if ((num >= 0xD800 && num <= 0xDFFF) || num >= 0x10FFFF) {
            return QChar(U'\uFFFD');
        }
        // Instead of a list of invalid codepoints, we try to reuse most of
        // QChar's features
        if (// 0x0001 to 0x0008
            (num >= 1 && num <= 8) ||
            // 0x000E to 0x001F
            (num >= 0xe && num <= 0x1f) ||
            // 0x007F to 0x009F
            (num >= 0x7f && num <= 0x9f) ||
            QChar::isNonCharacter(num)) {
            return QString();
        }
        return QChar(num);
    } else {
        // named charref
        QByteArray key = s.toUtf8();
        QString resolved = lookUpHtml5(key);
        if (!resolved.isNull()) {
            return resolved;
        }
        // find the longest matching name (as defined by the standard)
        for (int x = key.length() - 1; x > 1; x--) {
            resolved = lookUpHtml5(key.left(x));
            if (!resolved.isNull()) {
                return resolved + s.mid(x);
            }
        }
        return '&' + s;
    }
}

} // namespace

namespace it {
namespace mardy {
namespace Html {

QString unescape(const QString &s)
{
    /* Convert all named and numeric character references (e.g. &gt;, &#62;,
     * &x3e;) in the string s to the corresponding unicode characters.
     * This function uses the rules defined by the HTML 5 standard
     * for both valid and invalid character references, and the list of
     * HTML 5 named character references defined in html.entities.html5.
     */
    if (!s.contains('&')) {
        return s;
    }
    // Qt misses an equivalent for re.sub(), so...
    QXmlStreamReader reader; // only used to resolve entities
    QString unescaped;
    int processedChars = 0;
    QRegularExpressionMatchIterator i = s_charRef.globalMatch(s);
    while (i.hasNext()) {
        QRegularExpressionMatch m = i.next();
        unescaped += s.midRef(processedChars, m.capturedStart(0) - processedChars);
        unescaped += replaceCharRef(m);
        processedChars = m.capturedEnd(1);
    }
    return unescaped + s.midRef(processedChars);
}

}}} // namespace
