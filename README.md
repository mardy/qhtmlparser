[![pipeline status](https://gitlab.com/mardy/qhtmlparser/badges/master/pipeline.svg)](https://gitlab.com/mardy/qhtmlparser/-/commits/master)
[![coverage report](https://gitlab.com/mardy/qhtmlparser/badges/master/coverage.svg)](https://gitlab.com/mardy/qhtmlparser/-/commits/master)

QHtmlParser
===========

A tolerant HTML5 parser for C++/Qt applications based on Python's `html.parser`.


How to use it in your application
---------------------------------

At the moment, QHtmlParser is not provided as a library, but simply as a set of
files which one can import (possibly as a git submodule) into his/her project.

### Importing the source files

For applications built using [QBS](https://doc.qt.io/qbs/overview.html) this
can be done like this:

    import "../3rdparty/qhtmlparser/sources.qbs" as HtmlParserSources

    Product {
        // ...

        cpp.includePaths: [ htmlParserSources.prefix ]

        HtmlParserSources { id: htmlParserSources }
    }

You are very welcome to create a merge proposal adding support for your
favourite build system.

### Using the parser

QHtmlParser provides the `Parser` class, whose `feed()` method can be used to
feed data to the parser. The various handler virtual methods `handle*()` will
be called when the parser encounters the type of data specific to the handler.

```cpp
#include <Html/Parser>

namespace Html = it::mardy::Html;

class MyParser: public Html::Parser {
public:
    MyParser(): Html::Parser(Html::Parser::ConvertCharRefs) {
        // perform your own initializations
    }

    void handleStartTag(const QString &tag,
                        const Html::Parser::Attributes &attrs) override {
        qDebug() << "Found element" << tag;
    }

    ...
};

...

// Somewhere else in your code base:
MyParser parser;
parser.feed("<img src=\"hello.png\" />");
```

Since this parser is essentially a C++ translation of Python's `html.parser`
module, it might be a good idea to have a look at its
[documentation](https://docs.python.org/3/library/html.parser.html).


Licence
-------

QHtmlParser is licensed under the GPL3 licence.
