import qbs 1.0

Project {
    condition: project.buildTests

    Test {
        name: "html-parser-test"
        files: [
            "../src/Html/entities.h",
            "../src/Html/html.cpp",
            "../src/Html/html.h",
            "../src/Html/parser.cpp",
            "../src/Html/parser.h",
            "tst_html_parser.cpp",
        ]
    }

    CoverageClean {
        condition: project.enableCoverage
    }

    CoverageReport {
        condition: project.enableCoverage
        extractPatterns: [ '*/src/*.cpp' ]
    }
}
