/*
 * Copyright (C) 2021 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of QHtmlParser.
 *
 * QHtmlParser is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QHtmlParser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with QHtmlParser.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Html/Parser"

#include <QBuffer>
#include <QTest>
#include <QList>
#include <QVariantList>
#include <QVariantMap>

using namespace it::mardy::Html;

typedef QVector<QVariantList> EventList;

namespace QTest {

QString variantToString(const QVariant &v)
{
    QString s;
    if (v.canConvert<QString>()) {
        char *str = QTest::toString(v.toString());
        s = QString::fromUtf8(str);
        delete[] str;
    } else if (v.canConvert<QVariantMap>()) {
        const QVariantMap map = v.toMap();
        s += "{";
        QStringList entries;
        for (auto i = map.begin(); i != map.end(); i++) {
            entries.append(i.key() + ": " + variantToString(i.value()));
        }
        s += entries.join(", ");
        s += "}";
    }
    return s;
}

template<>
char *toString(const EventList &p)
{
    QByteArray ba = "{\n";
    for (const QVariantList &vlist: p) {
        ba += "  {";
        QStringList vAsStrings;
        for (const QVariant &v: vlist) {
            vAsStrings += variantToString(v);
        }
        ba += vAsStrings.join(", ").toUtf8();
        ba += "},\n";
    }
    ba += "}\n";
    return qstrdup(ba.data());
}

} // namespace QTest

class EventCollector: public Parser
{
public:
    EventCollector(Parser::Options options): Parser(options) {}

    const EventList &events() {
        flushData();
        return m_events;
    }

protected:
    void handleStartTag(const QString &tag,
                        const Attributes &attrs) override {
        append({"starttag", tag, serialize(attrs)});
    }

    void handleStartEndTag(const QString &tag,
                           const Attributes &attrs) override {
        append({"startendtag", tag, serialize(attrs)});
    }

    void handleEndTag(const QString &tag) override {
        append({"endtag", tag});
    }

    void handleComment(const QString &data) override {
        append({"comment", data});
    }

    void handleCharRef(const QString &name) override {
        append({"charref", name});
    }

    void handleData(const QString &data) override {
        m_bufferedData += data;
    }

    void handleDecl(const QString &decl) override {
        append({"decl", decl});
    }

    void handleEntityRef(const QString &name) override {
        append({"entityref", name});
    }

    void handlePi(const QString &data) override {
        append({"pi", data});
    }

    void handleUnknownDecl(const QString &data) override {
        append({"unknown decl", data});
    }

    QVariant serialize(const Parser::Attributes &attrs) const {
        QVariantMap map;
        for (const Parser::Attribute &attr: attrs) {
            map[attr.name] = attr.value;
        }
        return map;
    }

    void append(const QVariantList &event) {
        if (event[0].toString() != "data") {
            flushData();
        }
        m_events.append(event);
    }

    void flushData() {
        if (!m_bufferedData.isEmpty()) {
            m_events.append({"data", m_bufferedData});
            m_bufferedData.clear();
        }
    }

private:
    QString m_bufferedData;
    EventList m_events;
};

class EventCollectorExtra: public EventCollector
{
public:
    using EventCollector::EventCollector;

protected:
    void handleStartTag(const QString &tag,
                        const Attributes &attrs) override {
        EventCollector::handleStartTag(tag, attrs);
        append({"starttag_text", startTagText()});
    }
};

class HtmlParserTest: public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testBaseImplementation();

    void testSegmentation_data();
    void testSegmentation();

    void testEntities_data();
    void testEntities();

    void testEvents_data();
    void testEvents();

    void testCDataContent_data();
    void testCDataContent();

    void testStartTagText();

    void testConvertCharRefsDroppedText();
};

// From cpython/blob/3.9/Lib/test/test_htmlparser.py : test_simple_html
static const QByteArray simpleHtml = R"htmlDelimiter(
<!DOCTYPE html PUBLIC 'foo'>
<HTML>&entity;&#32;
<!--comment1a
-></foo><bar>&lt;<?pi?></foo<bar
comment1b-->
<Img sRc='Bar' isMAP>sample
text
&#x201C;
<!--comment2a-- --comment2b-->
</Html>
)htmlDelimiter";

void HtmlParserTest::testBaseImplementation()
{
    QTest::ignoreMessage(QtWarningMsg,
                         "HTML parser error: "
                         "\"unknown status keyword error in marked section\"");

    /* Just to check that the base implementation can be
     * instantiated and its handlers don't crash.
     */
    Parser parser(Parser::None);

    parser.feed(R"(
<!DOCTYPE html PUBLIC 'foo'>
<html>&entity;&#32;
<!-- comment ok -->
<![cdata]]>
<foo/>
<bar>text</bar>
<?pi>
<![error]]>
<!-- broken comment ->
<Img sRc='Bar'>sample</img>
</html>
)");
}

void HtmlParserTest::testSegmentation_data()
{
    QTest::addColumn<QByteArray>("htmlData");
    /* This is an array determining how many bytes will be delivered on each
     * feed() call. */
    QTest::addColumn<QVector<int>>("segments");

    QTest::addColumn<EventList>("expectedEvents");

    EventList simpleHtmlEvents {
        {"data", "\n"},
        {"decl", "DOCTYPE html PUBLIC 'foo'"},
        {"data", "\n"},
        {"starttag", "html", QVariantMap()},
        {"entityref", "entity"},
        {"charref", "32"},
        {"data", "\n"},
        {"comment", "comment1a\n-></foo><bar>&lt;<?pi?></foo<bar\ncomment1b"},
        {"data", "\n"},
        {"starttag", "img", QVariantMap{{"src", "Bar"}, {"ismap", QString()}}},
        {"data", "sample\ntext\n"},
        {"charref", "x201C"},
        {"data", "\n"},
        {"comment", "comment2a-- --comment2b"},
        {"data", "\n"},
        {"endtag", "html"},
        {"data", "\n"},
    };

    QTest::newRow("simpleHtml, 4k") <<
        simpleHtml <<
        QVector<int>((simpleHtml.size() / 4096) + 1, 4096) <<
        simpleHtmlEvents;

    QTest::newRow("simpleHtml, one byte") <<
        simpleHtml <<
        QVector<int>(simpleHtml.size(), 1) <<
        simpleHtmlEvents;

    QTest::newRow("simpleHtml, three bytes") <<
        simpleHtml <<
        QVector<int>((simpleHtml.size() / 3) + 1, 3) <<
        simpleHtmlEvents;
}

void HtmlParserTest::testSegmentation()
{
    QFETCH(QByteArray, htmlData);
    QFETCH(QVector<int>, segments);

    QFETCH(EventList, expectedEvents);

    QBuffer input;
    input.setData(htmlData);
    QVERIFY(input.open(QIODevice::ReadOnly));

    EventCollector parser(Parser::None);

    for (int segmentSize: segments) {
        QByteArray data = input.read(segmentSize);
        parser.feed(QString::fromUtf8(data));
    }
    parser.close();

    QCOMPARE(parser.events(), expectedEvents);
}

void HtmlParserTest::testEntities_data()
{
    QTest::addColumn<QString>("htmlData");
    QTest::addColumn<QString>("expectedData");

    QTest::newRow("no entities") <<
        "no entities at all" <<
        "no entities at all";

    QTest::newRow("numeric entities") <<
        "Some &#69;ntit&#x69;es: latin &#x9c;, "
        "surrogate &#xdf12;, invalid &#4;" <<
        QStringLiteral("Some Entities: latin œ,"
                       " surrogate \ufffd,"
                       " invalid ");

    QTest::newRow("missing semicolon") <<
        "No &#116erminator &sect " <<
        QStringLiteral("No terminator § ");

    QTest::newRow("named entities") <<
        "&Agrave; &amp; &boxvh; &nlE; &fjlig;" <<
        QStringLiteral("À & ┼ ≦̸ fj");

    QTest::newRow("named entities, with trailing chars") <<
        "I'm &notit; I tell you" <<
        QStringLiteral("I'm ¬it; I tell you");

    QTest::newRow("named entities, unparsable") <<
        "&asdfsg; what?" <<
        QStringLiteral("&asdfsg; what?");
}

void HtmlParserTest::testEntities()
{
    QFETCH(QString, htmlData);
    QFETCH(QString, expectedData);

    EventCollector parser(Parser::ConvertCharRefs);
    parser.feed(htmlData);
    parser.close();

    EventList events = parser.events();
    QCOMPARE(events.count(), 1);
    QCOMPARE(events[0].count(), 2);
    QCOMPARE(events[0][0].toString(), QString("data"));
    QCOMPARE(events[0][1].toString(), expectedData);
}

void HtmlParserTest::testEvents_data()
{
    QTest::addColumn<QString>("htmlData");
    QTest::addColumn<EventList>("expectedEvents");

    using EL = EventList;

    // test_processing_instruction_only
    QTest::newRow("pi 1") <<
        "<?processing instruction>" << EL {{ "pi", "processing instruction" }};
    QTest::newRow("pi 2") <<
        "<?processing instruction ?>" <<
        EL {{ "pi", "processing instruction ?" }};

    // test_startendtag
    QTest::newRow("startendtag 1") <<
        "<p/>" << EL {{ "startendtag", "p", QVariantMap() }};
    QTest::newRow("startendtag 2") << "<p></p>" <<
        EL {{ "starttag", "p", QVariantMap() }, { "endtag", "p" }};
    QTest::newRow("startendtag 3") << "<p><img src='foo' /></p>" <<
        EventList {
            { "starttag", "p", QVariantMap() },
            { "startendtag", "img", QVariantMap {{ "src", "foo" }} },
            { "endtag", "p" },
        };

    // test_comments
    QTest::newRow("comments") <<
        "<!-- I'm a valid comment -->"
        "<!--me too!-->"
        "<!------>"
        "<!---->"
        "<!----I have many hyphens---->"
        "<!-- I have a > in the middle -->"
        "<!-- and I have -- in the middle! -->" <<
        EventList {
            { "comment", " I'm a valid comment " },
            { "comment", "me too!" },
            { "comment", "--" },
            { "comment", "" },
            { "comment", "--I have many hyphens--" },
            { "comment", " I have a > in the middle " },
            { "comment", " and I have -- in the middle! " },
        };

    // test_condcoms
    QTest::newRow("condcoms") <<
        "<!--[if IE & !(lte IE 8)]>aren't<![endif]-->"
        "<!--[if IE 8]>condcoms<![endif]-->"
        "<!--[if lte IE 7]>pretty?<![endif]-->" <<
        EventList {
            { "comment", "[if IE & !(lte IE 8)]>aren't<![endif]" },
            { "comment", "[if IE 8]>condcoms<![endif]" },
            { "comment", "[if lte IE 7]>pretty?<![endif]" },
        };

    // test_tolerant_parsing
    QTest::newRow("tolerant parsing") <<
        "<html <html>te>>xt&a<<bc</a></html>\n"
        R"(<img src="URL><//img></html</html>)" <<
        EventList {
            { "starttag", "html", QVariantMap {{ "<html", QString() }} },
            { "data", "te>>xt" },
            { "entityref", "a" },
            { "data", "<" },
            { "starttag", "bc<", QVariantMap {{ "a", QString() }} },
            { "endtag", "html" },
            { "data", "\n<img src=\"URL>" },
            { "comment", "/img" },
            { "endtag", "html<" },
        };

    // test_starttag_junk_chars(self):
    QTest::newRow("junk 1") << "</>" << EL {};
    QTest::newRow("junk 2") << "</$>" << EL {{ "comment", "$" }};
    QTest::newRow("junk 3") << "</" << EL {{ "data", "</" }};
    QTest::newRow("junk 4") << "</a" << EL {{ "data", "</a" }};
    QTest::newRow("junk 5") << "<a<a>" << EL {{ "starttag", "a<a", QVariantMap() }};
    QTest::newRow("junk 6") << "</a<a>" << EL {{ "endtag", "a<a" }};
    QTest::newRow("junk 7") << "<!" << EL {{ "data", "<!" }};
    QTest::newRow("junk 8") << "<a" << EL {{ "data", "<a" }};
    QTest::newRow("junk 9") << "<a foo='bar'" << EL {{ "data", "<a foo='bar'" }};
    QTest::newRow("junk 10") << "<a foo='bar" << EL {{ "data", "<a foo='bar" }};
    QTest::newRow("junk 11") << "<a foo='>'" << EL {{ "data", "<a foo='>'" }};
    QTest::newRow("junk 12") << "<a foo='>" << EL {{ "data", "<a foo='>" }};
    QTest::newRow("junk 13") << "<a$>" << EL {{ "starttag", "a$", QVariantMap() }};
    QTest::newRow("junk 14") << "<a$b>" << EL {{ "starttag", "a$b", QVariantMap() }};
    QTest::newRow("junk 15") << "<a$b/>" << EL {{ "startendtag", "a$b", QVariantMap() }};
    QTest::newRow("junk 16") << "<a$b  >" << EL {{ "starttag", "a$b", QVariantMap() }};
    QTest::newRow("junk 17") << "<a$b  />" << EL {{ "startendtag", "a$b", QVariantMap() }};

    // test_slashes_in_starttag
    QTest::newRow("slashes in start tags 1") <<
        R"(<img width=902 height=250px )"
        R"(src="/sites/default/files/images/homepage/foo.jpg" )"
        R"(/*what am I doing here*/ />)" <<
        EventList {
            { "startendtag", "img", QVariantMap {
                { "width", "902" }, { "height", "250px" },
                { "src", "/sites/default/files/images/homepage/foo.jpg" },
                { "*what", QString() }, { "am", QString() }, { "i", QString() },
                { "doing", QString() }, { "here*", QString() },
            }},
        };
    QTest::newRow("slashes in start tags 2") <<
        R"(<a / /foo/ / /=/ / /bar/ / />)"
        R"(<a / /foo/ / /=/ / /bar/ / >)" <<
        EventList {
            { "startendtag", "a", QVariantMap {
                { "foo", QString() }, { "=", QString() }, { "bar", QString() },
            }},
            { "starttag", "a", QVariantMap {
                { "foo", QString() }, { "=", QString() }, { "bar", QString() },
            }},
        };
    QTest::newRow("slashes in start tags 3") <<
        R"(<meta><meta / ><meta // ><meta / / >)"
        R"(<meta/><meta /><meta //><meta//>)" <<
        EventList {
            { "starttag", "meta", QVariantMap() },
            { "starttag", "meta", QVariantMap() },
            { "starttag", "meta", QVariantMap() },
            { "starttag", "meta", QVariantMap() },
            { "startendtag", "meta", QVariantMap() },
            { "startendtag", "meta", QVariantMap() },
            { "startendtag", "meta", QVariantMap() },
            { "startendtag", "meta", QVariantMap() },
        };

    // test_declaration_junk_chars
    QTest::newRow("declaration junk chars") <<
        "<!DOCTYPE foo $ >" <<
        EL {{ "decl", "DOCTYPE foo $ " }};

    // test_illegal_declarations
    QTest::newRow("test illegal declaration") <<
        R"(<!spacer type="block" height="25">)" <<
        EL {{ "comment", R"(spacer type="block" height="25")" }};

    // test_invalid_end_tags
    QTest::newRow("invalid end tags") <<
        R"(<br></label</p><br></div end tmAd-leaderBoard><br></<h4><br>)"
        "</li class=\"unit\"><br></li\r\n\t\t\t\t\t\t</ul><br></><br>" <<
        EventList {
            { "starttag", "br", QVariantMap() },
            // < is part of the name, / is discarded, p is an attribute
            { "endtag", "label<" },
            { "starttag", "br", QVariantMap() },
            // text and attributes are discarded
            { "endtag", "div" },
            { "starttag", "br", QVariantMap() },
            // comment because the first char after </ is not a-zA-Z
            { "comment", "<h4" },
            { "starttag", "br", QVariantMap() },
            // attributes are discarded
            { "endtag", "li" },
            { "starttag", "br", QVariantMap() },
            // everything till ul { included } is discarded
            { "endtag", "li" },
            { "starttag", "br", QVariantMap() },
            // </> is ignored
            { "starttag", "br", QVariantMap() },
        };

    // test_broken_invalid_end_tag
    QTest::newRow("broken invalid end tag") <<
        R"(<b>This</b attr=">"> confuses the parser)" <<
        EventList {
            { "starttag", "b", QVariantMap() },
            { "data", "This" },
            { "endtag", "b" },
            { "data", R"("> confuses the parser)" },
        };

    // test_correct_detection_of_start_tags
    QTest::newRow("start tags") <<
        R"(<div style=""    ><b>The <a href="some_url">rain</a> )"
        R"(<br /> in <span>Spain</span></b></div>)" <<
        EventList {
            { "starttag", "div", QVariantMap{{ "style", "" }} },
            { "starttag", "b", QVariantMap() },
            { "data", "The " },
            { "starttag", "a", QVariantMap{{ "href", "some_url" }} },
            { "data", "rain" },
            { "endtag", "a" },
            { "data", " " },
            { "startendtag", "br", QVariantMap() },
            { "data", " in " },
            { "starttag", "span", QVariantMap() },
            { "data", "Spain" },
            { "endtag", "span" },
            { "endtag", "b" },
            { "endtag", "div" }
        };

    // test_EOF_in_charref
    QTest::newRow("EOF charref 1") << "a&" << EL {{ "data", "a&" }};
    QTest::newRow("EOF charref 2") << "a&b"<< EL {{ "data", "ab" }};
    QTest::newRow("EOF charref 3") << "a&b " <<
        EL {{ "data", "a" }, { "entityref", "b" }, { "data", " " }};
    QTest::newRow("EOF charref 4") << "a&b;" <<
        EL {{"data", "a" }, { "entityref", "b" }};

    // test_broken_comments
    QTest::newRow("broken comments") <<
        "<! not really a comment >"
        "<! not a comment either -->"
        "<! -- close enough -->"
        "<!><!<-- this was an empty comment>"
        "<!!! another bogus comment !!!>" <<
        EventList {
            { "comment", " not really a comment " },
            { "comment", " not a comment either --" },
            { "comment", " -- close enough --" },
            { "comment", "" },
            { "comment", "<-- this was an empty comment" },
            { "comment", "!! another bogus comment !!!" },
        };

    // test_broken_condcoms
    QTest::newRow("broken condcoms") <<
        R"(<![if !(IE)]>broken condcom<![endif]>)"
        R"(<![if ! IE]><link href="favicon.tiff"/><![endif]>)"
        R"(<![if !IE 6]><img src="firefox.png" /><![endif]>)"
        R"(<![if !ie 6]><b>foo</b><![endif]>)"
        R"(<![if (!IE)|(lt IE 9)]><img src="mammoth.bmp" /><![endif]>)" <<
        EventList {
            { "unknown decl", "if !(IE)"},
            { "data", "broken condcom" },
            { "unknown decl", "endif" },
            { "unknown decl", "if ! IE" },
            { "startendtag", "link", QVariantMap{{"href", "favicon.tiff"}} },
            { "unknown decl", "endif" },
            { "unknown decl", "if !IE 6" },
            { "startendtag", "img", QVariantMap{{"src", "firefox.png"}} },
            { "unknown decl", "endif" },
            { "unknown decl", "if !ie 6" },
            { "starttag", "b", QVariantMap() },
            { "data", "foo" },
            { "endtag", "b" },
            { "unknown decl", "endif" },
            { "unknown decl", "if (!IE)|(lt IE 9)"},
            { "startendtag", "img", QVariantMap{{"src", "mammoth.bmp"}} },
            { "unknown decl", "endif" },
        };
}

void HtmlParserTest::testEvents()
{
    QFETCH(QString, htmlData);
    QFETCH(EventList, expectedEvents);

    EventCollector parser(Parser::None);
    parser.feed(htmlData);
    parser.close();

    QCOMPARE(parser.events(), expectedEvents);
}

void HtmlParserTest::testCDataContent_data()
{
    QTest::addColumn<QString>("content");

    // test_cdata_content
    QTest::newRow("1") << "<!-- not a comment --> &not-an-entity-ref;";
    QTest::newRow("2") << "<not a='start tag'>";
    QTest::newRow("3") << R"(<a href="" /> <p> <span></span>)";
    QTest::newRow("4") << R"(foo = "</scr" + "ipt>";)";
    QTest::newRow("5") << R"("foo = "</SCRIPT" + ">";)";
    QTest::newRow("6") << "foo = <\n/script> ";
    QTest::newRow("7") << R"(<!-- document.write("</scr" + "ipt>"); -->)";
    QTest::newRow("8") <<
        "\n//<![CDATA[\n"
        "document.write('<s'+'cript type=\"text/javascript\" "
        "src=\"http://www.example.org/r='+new "
        "Date().getTime()+'\"><\\/s'+'cript>');\n//]]>";
    QTest::newRow("9") << "\n<!-- //\nvar foo = 3.14;\n// -->\n";
    QTest::newRow("10") << R"(foo = "</sty" + "le>";)";
    QTest::newRow("11") << "<!-- \u2603 -->";
}

void HtmlParserTest::testCDataContent()
{
    QFETCH(QString, content);

    const QStringList elements = {
        "script", "style", "SCRIPT", "STYLE", "Script", "Style",
    };

    for (const QString &element: elements) {
        const QString elementLower = element.toLower();

        QString htmlData = QString("<%1>%2</%1>").arg(element, content);

        EventCollector parser(Parser::None);
        parser.feed(htmlData);
        parser.close();

        EventList expectedEvents {
            { "starttag", elementLower, QVariantMap() },
            { "data", content },
            { "endtag", elementLower },
        };
        QCOMPARE(parser.events(), expectedEvents);
    }
}

void HtmlParserTest::testStartTagText()
{
    // test_get_starttag_text
    QString s = "<foo:bar   \n   one=\"1\"\ttwo=2   >";

    EventCollectorExtra parser(Parser::None);
    parser.feed(s);
    parser.close();

    EventList expectedEvents {
        { "starttag", "foo:bar",
            QVariantMap {{ "one", "1" }, { "two", "2" }} },
        { "starttag_text", "<foo:bar   \n   one=\"1\"\ttwo=2   >" },
    };
    QCOMPARE(parser.events(), expectedEvents);
};

/* def test_convert_charrefs_dropped_text(self):
 *     # #23144: make sure that all the events are triggered when
 *     # convert_charrefs is True, even if we don't call .close()
 */
void HtmlParserTest::testConvertCharRefsDroppedText()
{
    EventCollector parser(Parser::ConvertCharRefs);
    parser.feed("foo <a>link</a> bar &amp; baz");

    EventList expectedEvents {
        {"data", "foo "},
        {"starttag", "a", QVariantMap()},
        {"data", "link"},
        {"endtag", "a"},
        {"data", " bar & baz"},
    };
    QCOMPARE(parser.events(), expectedEvents);
}

QTEST_GUILESS_MAIN(HtmlParserTest)

#include "tst_html_parser.moc"
